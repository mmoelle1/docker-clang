docker-clang
============

[![pipeline status](https://gitlab.com/mmoelle1/docker-clang/badges/master/pipeline.svg)](https://gitlab.com/mmoelle1/docker-clang/commits/master)

Repository contains a customizable `Dockerfile` based on `ubuntu:16.04`, `18.04`, 
and `20.04` with **Clang C/C++ compiler environment**. The generated docker images
moreover contain the tools `cmake`, `doxygen`, `git`, `ninja`, `svn`, `wget` 
and the libraries `arrayfire`, `blas`, `boost`, and `lapack`. All containers
contain the Intel OpenCL driver for Intel CPUs.

Available docker images:

**Ubuntu 16.04 (LTS)**
- [registry.gitlab.com/mmoelle1/docker-clang:4.0ubuntu16.04](registry.gitlab.com/mmoelle1/docker-clang:4.0ubuntu16.04)
- [registry.gitlab.com/mmoelle1/docker-clang:5.0ubuntu16.04](registry.gitlab.com/mmoelle1/docker-clang:5.0ubuntu16.04)
- [registry.gitlab.com/mmoelle1/docker-clang:6.0ubuntu16.04](registry.gitlab.com/mmoelle1/docker-clang:6.0ubuntu16.04)
- [registry.gitlab.com/mmoelle1/docker-clang:8ubuntu16.04](registry.gitlab.com/mmoelle1/docker-clang:8ubuntu16.04)

**Ubuntu 18.04 (LTS)**
- [registry.gitlab.com/mmoelle1/docker-clang:6.0ubuntu18.04](registry.gitlab.com/mmoelle1/docker-clang:6.0ubuntu18.04)
- [registry.gitlab.com/mmoelle1/docker-clang:7ubuntu18.04](registry.gitlab.com/mmoelle1/docker-clang:7ubuntu18.04)
- [registry.gitlab.com/mmoelle1/docker-clang:8ubuntu18.04](registry.gitlab.com/mmoelle1/docker-clang:8ubuntu18.04)

**Ubuntu 20.04 (LTS)**
- [registry.gitlab.com/mmoelle1/docker-clang:8ubuntu20.04](registry.gitlab.com/mmoelle1/docker-clang:8ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-clang:9ubuntu20.04](registry.gitlab.com/mmoelle1/docker-clang:9ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-clang:10ubuntu20.04](registry.gitlab.com/mmoelle1/docker-clang:10ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-clang:11ubuntu20.04](registry.gitlab.com/mmoelle1/docker-clang:11ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-clang:12ubuntu20.04](registry.gitlab.com/mmoelle1/docker-clang:12ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-clang:13ubuntu20.04](registry.gitlab.com/mmoelle1/docker-clang:13ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-clang:14ubuntu20.04](registry.gitlab.com/mmoelle1/docker-clang:14ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-clang:15ubuntu20.04](registry.gitlab.com/mmoelle1/docker-clang:15ubuntu20.04)
- [registry.gitlab.com/mmoelle1/docker-clang:16ubuntu20.04](registry.gitlab.com/mmoelle1/docker-clang:16ubuntu20.04)
